/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package delete

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

var (
	targetTemplatePath *string
)

func deleteTemplates(target *string) error {

	if *target == "" {
		err := errors.New("--target: target templates path must be specified")
		return err
	}

	d, err := os.Open(*target)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer d.Close()

	files, err := d.Readdir(-1)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, file := range files {
		if file.Mode().IsRegular() {
			if filepath.Ext(file.Name()) == ".md" {
				os.Remove(*target + "/" + file.Name())
				if err != nil {
					fmt.Println(err)
				}
				fmt.Println("Deleted ", file.Name())
			}
		}
	}

	return nil
}

// templatesCmd represents the templates command
var templatesCmd = &cobra.Command{
	Use:   "templates",
	Short: "Subcommand for operations with templates",
	Long: `You can use this subcommand for delete templates for issues in Gitlab.
		
	delete: cli will delete issue_templates in target (--target) path`,
	Run: func(cmd *cobra.Command, args []string) {
		//var labels [][]string

		err := deleteTemplates(targetTemplatePath)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println("Done!")
		}
	},
}

func init() {

	targetTemplatePath = new(string)
	templatesCmd.Flags().StringVarP(targetTemplatePath, "target", "t", "", `Specify target files parh for template: --target="/home/user/task-board/.gitlab/issue_templates"`)

	DeleteCmd.AddCommand(templatesCmd)
}
