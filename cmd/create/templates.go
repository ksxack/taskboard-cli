/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package create

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

type listLabels struct {
	labels []string
	next   *listLabels
}

var (
	labels1, labels2, labels3, labels4, labels5 *[]string

	sourceTemplatePath *string
	targetTemplatePath *string
)

func crateTemplates(ll *listLabels, source, target *string) error {

	if *source == "" {
		err := errors.New("--source: source template path must be specified")
		return err
	}

	if *target == "" {
		err := errors.New("--target: target templates path must be specified")
		return err
	}

	b, err := os.ReadFile(*source) // just pass the file name
	if err != nil {
		return err
	}

	labelCombinations := new([][]string)
	combineLabels(ll, labelCombinations, []string{})

	for _, lblSet := range *labelCombinations {

		targetFileName := ""
		strSource := string(b) + "\n\n"

		for _, lbl := range lblSet {
			strSource += fmtLabel(lbl)
			targetFileName += lbl + "_"
		}
		if strings.Contains(targetFileName, "/") {
			targetFileName = strings.ReplaceAll(targetFileName, "/", "-")
		}

		file, err := os.Create(*target + "/" + targetFileName[:len(targetFileName)-1] + ".md")

		if err != nil {
			return err
		}

		defer file.Close()

		_, err = file.WriteString(strSource)

		if err != nil {
			return err
		}

	}

	return nil
}

// templatesCmd represents the templates command
var templatesCmd = &cobra.Command{
	Use:   "templates",
	Short: "Subcommand for operations with templates",
	Long: `You can use this subcommand for create templates for issues in Gitlab.
	
	create: cli will read your template file (specified with --source) and will combine all pasted lables (via --label). Then it will be stored in target path (--target).`,
	Run: func(cmd *cobra.Command, args []string) {
		//var labels [][]string

		head := new(bool)
		*head = true

		ll := new(listLabels)

		addLabel(ll, *labels1, head)
		addLabel(ll, *labels2, head)
		addLabel(ll, *labels3, head)
		addLabel(ll, *labels4, head)
		addLabel(ll, *labels5, head)

		err := crateTemplates(ll, sourceTemplatePath, targetTemplatePath)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			fmt.Println("Done!")
		}
	},
}

func init() {
	labels1 = new([]string)
	labels2 = new([]string)
	labels3 = new([]string)
	labels4 = new([]string)
	labels5 = new([]string)
	sourceTemplatePath = new(string)
	targetTemplatePath = new(string)

	templatesCmd.Flags().StringSliceVarP(labels1, "labels1", "", []string{}, `Array of labels1: --labels="Incident,Ticket,Error"`)
	templatesCmd.Flags().StringSliceVarP(labels2, "labels2", "", []string{}, `Array of labels2: --labels="P1,P2,P3,P4"`)
	templatesCmd.Flags().StringSliceVarP(labels3, "labels3", "", []string{}, `Array of labels3: --labels="Golang, Java, Python"`)
	templatesCmd.Flags().StringSliceVarP(labels4, "labels4", "", []string{}, `Array of labels4: --labels="Backend, CI/CD, Infra"`)
	templatesCmd.Flags().StringSliceVarP(labels4, "labels5", "", []string{}, `Array of labels5: --labels="Intech, Bank"`)
	templatesCmd.Flags().StringVarP(sourceTemplatePath, "source", "s", "", `Specify source file parh for template: --source="/home/user/issue_template.txt"`)
	templatesCmd.Flags().StringVarP(targetTemplatePath, "target", "t", "", `Specify path to place generated templates: --target="/home/user/task-board/.gitlab/issue_templates"`)

	CreateCmd.AddCommand(templatesCmd)
}

func fmtLabel(lbl string) string {
	return ` ~"` + lbl + `" 
` + `/label ~"` + lbl + `" 
`
}

func addLabel(ll *listLabels, v []string, head *bool) int {
	if len(v) == 0 {
		return 0
	}
	//null := ListNode{0, nil}
	if *head {
		ll.labels = v
		*head = false
		return -1
	}
	if ll.next == nil {
		ll.next = &listLabels{v, nil}
		return -2
	}

	return addLabel(ll.next, v, head)

}

func combineLabels(ll *listLabels, labelCombinations *[][]string, s []string) [][]string {

	if len(ll.labels) == 0 && ll.next != nil {
		return combineLabels(ll.next, labelCombinations, []string{})
	}

	if ll.next != nil {
		for _, v := range ll.labels {
			v := append(s, v)
			combineLabels(ll.next, labelCombinations, v)
		}
	} else {
		for _, v := range ll.labels {
			vv := append(s, v)
			*labelCombinations = append(*labelCombinations, vv)
		}
	}

	return nil
}
