/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package create

import (
	"github.com/spf13/cobra"
)

// CreateCmd represents the create command
var CreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Collecton of commands subpalletes, use taskboard-cli create templates -h",
	Long: `Collecton of commands subpalletes to create templates and something else in the future.
	
Use "taskboard-cli create templates -h" for get info about creating issue templates.`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {

}
