# taskboard-cli

Simple cli for creating issue templates. This cli can use source template file and combinate labels given to cli. 

## Installation 

```
git clone https://gitlab.com/ksxack/taskboard-cli.git
cd taskboard-cli
sudo go build -o /usr/local/bin/taskboard-cli main.go 
chmod +x /usr/local/bin/taskboard-cli
```

## Usage

For example, you have source file for templates: 

![](/assets/1.jpg)

cli command:
```bash
taskboard-cli create templates --source="/Users/kazbektokaev/go/src/gitlab.com/telegram-bts/tasks/.gitlab/generator/template_development.txt" --target="/Users/kazbektokaev/go/src/gitlab.com/telegram-bts/tasks/.gitlab/issue_templates" --labels1="\"Разработка\"" --labels2="\"Бот\",\"CICD\",\"Плейбуки\",\"Инфраструктура\",\"Другое\"" --labels3 "\"Приоритет Низкий\",\"Приоритет Средний\",\"Приоритет Высокий\",\"Приоритет Критический\""  
```

Result:

![](/assets/2.jpg)

This cli is useful for creating issue boards, like this: 

![](/assets/4.jpg)

![](/assets/5.jpg)

But today this cli can't generate tables. 

So I've made 80 issue templates in 2 minutes but I still need to manually put in into my Wiki-table. I spent about 20 minutes to create this wiki-form.

![](/assets/3.jpg)