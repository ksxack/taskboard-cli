/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/telegram-bts/taskboard-cli/cmd"

func main() {
	cmd.Execute()
}
